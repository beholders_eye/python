# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi python [ blacklist=3.10 multibuild=false ] cmake

MY_PN="pyside-setup-opensource-src"

export_exlib_phases src_prepare

SUMMARY="CPython-based binding code generator for C or C++ libraries"
DESCRIPTION="
It uses an ApiExtractor library to parse the C or C++ headers and get the type
information, using Clang. The library is used for PySide2 Qt5 bindings but can
also be used to parse non-Qt projects."

HOMEPAGE+=" https://doc.qt.io/qtforpython/${PN}/"
DOWNLOADS="mirror://qt/official_releases/QtForPython/pyside2/PySide2-${PV}-src/${MY_PN}-${PV}.tar.xz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

QT_VERSION="$(ever range 1-2)"

DEPENDENCIES="
    build+run:
        dev-lang/clang:=[>=3.9]
        dev-lang/llvm:=[>=3.9]
        dev-libs/libxml2:2.0[>=2.6.32]
        dev-libs/libxslt[>=1.1.19]
        dev-python/numpy[python_abis:*(-)?]
        x11-libs/qtbase:5[~>${QT_VERSION}]
        x11-libs/qtxmlpatterns:5[~>${QT_VERSION}]
"

# A lot of tests fail
RESTRICT="test"

CMAKE_SOURCE="${WORKBASE}"/${MY_PN}-${PV}/sources/${PN}

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPYTHON_EXECUTABLE=${PYTHON}
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

shiboken2_src_prepare() {
    cmake_src_prepare

    edo sed -e "s:share/man/man1:/usr/&:" -i doc/CMakeLists.txt
}

